# Web

[[toc]]

## Text fragments

[https://developer.mozilla.org/en-US/docs/Web/URI/Fragment/Text_fragments#:~:text=without relying on the presence of IDs](https://developer.mozilla.org/en-US/docs/Web/URI/Fragment/Text_fragments#:~:text=without%20relying%20on%20the%20presence%20of%20IDs)

## Unicode/UTF-8

UTF-8 is a character encoding standard used for electronic communication. Defined by the [Unicode](https://home.unicode.org/) Standard, the name is derived from Unicode Transformation Format – 8-bit.

Full list of unicode symbols https://symbl.cc/en/unicode-table/.

### Warning emoji

Some symbols have textual representations such as the [warning emoji](https://symbl.cc/en/26A0-warning-emoji/) (⚠️). This can be displayed as the textual represenation (&#x26a0;&#xFE0E;) when not desired or visa-versa.

You can append a variation selector to force text ([`U+FE0E`](https://symbl.cc/en/FE0E/)) or emoji ([`U+FE0F`](https://symbl.cc/en/FE0F/)).

<p>&#x26A0;&#xFE0E; or &#x26A0;&#xFE0F;</p>

```html
<p>&#x26A0;&#xFE0E; or &#x26A0;&#xFE0F;</p>
```

Or to use in JS `\u26A0\uFE0E` and `\u26A0\uFE0F`.

## CSS

### Masked text

```css
h1 {
  background-clip: text;
  background-image: url(image.svg);
  background-position: 0 -0.25rem;
  background-repeat: no-repeat;
  color: transparent;
}
```

## Resources

- [Omatsuri](https://omatsuri.app/). Colour shades, gradients, svg tools, symbols, and other tools.
