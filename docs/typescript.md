# TypeScript

[[toc]]

## Use an array of values as types

```ts
// Cast your array as const
const GENOMES = ["steg", "trike", "rex"] as const;
// Genomes type from array
type Genomes = typeof STATE[number];
```

## Cast event target explicitly

TypeScript cannot properly deduce what the target type is from an Event, resulting in the error: `Property '...' does not exist on type 'EventTarget'.`. Manually assign the event target to the correct type:

```ts
const handleEvent = (event: Event) => {
  const target = event.target as EventTarget;
  // ...
};
```

## Catching errors (`try/catch`)

With TypeScript, errors are typed natively as `unknown` which can be tricky when knowing how to deal with them. Here is a suggested method:

```ts
try {
  console.log("Calling API.");
  const response = await fetch("/api");
  if (!response.ok) {
    throw new Error("There has been an error with requesting the API.");
  }
  const data = await response.json();
  if ("error" in data) {
    throw new Error(data.error);
  }
  console.log("API success.", { data });
} catch (error) {
  // @note: `error: unknown` by default.
  if (error instanceof Error) {
    console.error(error.message);
  } else {
    console.error("There has been an unknown error.");
  }
} finally {
  console.log("API call complete.");
}
```
