# Learning

[[toc]]

## Learning resources

- [Front end masters](https://frontendmasters.com/courses/good-parts-javascript-web/)

## Reference

- [33 concepts every JS dev should know](https://github.com/leonardomso/33-js-concepts)

## Articles

- [React internals](http://www.mattgreer.org/articles/react-internals-part-one-basic-rendering/)
- [How to GraphQL](https://www.howtographql.com/?v1)
- [GraphQL/React course](https://learn.hasura.io/graphql/react/introduction)
- [React/redux style guide](https://github.com/iraycd/React-Redux-Styleguide)
