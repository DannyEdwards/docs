# Database

[[toc]]

## MySQL

Logging into the Mysql command line client.

> mysql -u _username_ -p

Importing a database from an SQL file.

> mysql -u _username_ -p _database_ < _filename_

Exporting a table to a file (MySQL dump).

> mysqldump -u _username_ -p _database_ [_table_] > _filename_

```bash
mysqldump -u dnedry -p jurassicpark dinosaurs > genomes.sql
```

### PhpMyAdmin

Download the latest PhpMyAdmin, then extract and place the folder in your webroot (`/Library/WebSever/Documents`).
