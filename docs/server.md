# Server

[[toc]]

## Apache

### httpd.conf

Usually found at `/etc/apache2/httpd.conf`.

### Restarting Apache

<code-group>
<code-block title="Mac">
```bash
sudo apachectrl restart
```
</code-block>

<code-block title="Linux">
```bash
service apache2 restart
```
</code-block>
</code-group>

### List directories in localhost

In your `httpd.conf` add the `Indexes` option to the directory config.

```apacheconf
<Directory "/Library/WebServer/Documents">
  Options FollowSymLinks Multiviews Indexes
  MultiviewsMatch Any
  AllowOverride None
  Require all granted
</Directory>
```

### Get rid of that ‘Could not reliably determine...;’ error

Error when restarting Apache.

```bash
sudo service apache2 restart
 * Restarting web server apache2
 AH00558: apache2: Could not reliably determine the server's fully qualified domain name, using [Mac address]. Set the 'ServerName' directive globally to suppress this message
```

Add `ServerName localhost` into `httpd.conf` after `ServerAdmin`.

### Virtual hosts

Copy the default site config in `/etc/apache2/sites-available`.

```bash
cd /etc/apache2/sites-available
cp 000-default.conf new-site.conf
sudo vim new-site.conf

# Edit the file as necessary.

sudo a2ensite new-site
```

### Enable mod_rewrite (URL rewriting)

Uncomment the following line in `httpd.conf`.

```apacheconf
LoadModule rewrite_module libexec/apache2/mod_rewrite.so
```

## TLS (Transport Layer Security) / SSL

### Certbot

[Certbot](https://certbot.eff.org) is a command line tool for automating certificate generation with Let's Encrypt.

For information on current certificates run `certbot certificates`.

Use `certbot` in [manual](https://certbot.eff.org/docs/using.html#manual) mode if you don't have control of the server but can update files i.e. [GitLab pages](https://about.gitlab.com/2016/04/11/tutorial-securing-your-gitlab-pages-with-tls-and-letsencrypt/).

By default, the `http` challenge will be used. You can use the `dns` challenge by passing the `--preferred-challenges` arg, which is easier to manage.

```bash
certbot certonly -a manual -d [domain] --preferred-challenges dns
```

This method requires you to add a `TXT` record to your DNS. For [Namecheap](https://www.namecheap.com/) text records must go under the `_acme-challenge` host option.

| Type       | Host              | Value                       |
| ---------- | ----------------- | --------------------------- |
| TXT Record | `_acme-challenge` | `1A2B3C4D5E6F7G8H9I-ABxyMN` |

### Gitlab Pages

For GitLab pages, the `TXT` record must be under the `@` host option.

`_gitlab-pages-verification-code.yourdomain.co.uk TXT gitlab-pages-verification-code=1a2b3c4d5e6f7g8h9i`

| Type       | Host | Value                                               |
| ---------- | ---- | --------------------------------------------------- |
| TXT Record | `@`  | `gitlab-pages-verification-code=1a2b3c4d5e6f7g8h9i` |

After you have updated your cert, you will also have to update the domain by editing it in the Settings/Pages section of your GitLab project.

A `gitlab-ci.yml` for static HTML.

```yml
image: alpine:latest

pages:
  stage: deploy
  script:
    - echo 'Nothing to do...'
  artifacts:
    paths:
      - public
  only:
    - master
```

## PHP

### Load the PHP module

You might find after setting up a new machine or updating OSX your PHP files are not executing, this is because the PHP module is not being told to load in your new `httpd.conf` file. To get it going uncomment the following line:

```apacheconf
LoadModule php5_module libexec/apache2/libphp5.so
```

### Multiple versions of PHP

You can manage PHP versions with Brew. To install a different version of php run `brew install php@5.5`. To use the alternate versions you have to create the link, this is also done with brew.

```bash
# First unlink any versions you already have linked
brew unlink php

# Link the new version (you may need to use the `force` flag)
brew link --force php@5.5
```

Pay attention to the console towards the end of the Brew installation, you will have to point Apache to the alternate version by editing `http.conf`.

```apacheconf
LoadModule php7_module /usr/local/opt/php@7.1/lib/httpd/modules/libphp7.so
```

Restart apache `sudo apachectl restart`.
