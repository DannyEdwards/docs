# Vim

[[toc]]

## Tutor

Try the in built Vim tutorial from the command line with `vimtutor`.

## Saving and quitting

In normal mode (default) save with `:w`. Quit with `:q`. Commands can be combined, i.e. to save and quit you can type `:wq` followed by `Enter`.

To enter insert mode, hit `i`. To escape insert mode hit `esc`.

## Movement

Cursor movement can be carried out with the arrow keys or, if you're feeling particularily leet, you can use the `h` (←), `j` (↓), `k` (↑), and `l` (→) keys.

```bash
        ↑
        k              Hint:  The h key is at the left and moves left.
  ← h       l →               The l key is at the right and moves right.
        j                     The j key looks like a down arrow.
        ↓
```

## Navigating

| Action                           | Keys                      |
| -------------------------------- | ------------------------- |
| End of line                      | `$`                       |
| Start of line                    | `0`                       |
| Start of line (after whitespace) | `^`                       |
| Next/previous word               | `w`/`b`                   |
| Next/previous end of word        | `e`/`ge`                  |
| Half page up/down                | `Ctrl` + `u`/`Ctrl` + `d` |
| First/last line                  | `gg`/`G`                  |
| Nearest matching parenthesis     | `%`                       |
| Previous `(`/`{`/`<`             | `[(`/`[{`/`[<`            |
| Next `(`                         | `](`                      |

## Clipboard

| Action            | Keys |
| ----------------- | ---- |
| Delete character  | `x`  |
| Delete word       | `dw` |
| Delete line (cut) | `dd` |
| Yank line (copy)  | `yy` |
| Paste             | `p`  |

## Editing

| Action                           | Keys         |
| -------------------------------- | ------------ |
| Undo                             | `u`          |
| Redo                             | `ctrl` + `r` |
| Jump to end of line              | `$`          |
| Jump to beginning of line        | `0`          |
| Delete to enf of line and insert | `C`          |
