# Comamnd line

[[toc]]

## Setting up

"It's a Unix system, I know this!"

Install [Oh my zsh](https://ohmyz.sh/) and [dotfiles](https://gitlab.com/DannyEdwards/dotfiles).

A good online manual for Linux is [die.net](https://linux.die.net/)

## Navigation shortcuts

| Action                      |     Keys            |
| --------------------------- | :-----------------: |
| Delete to end of line       | `ctrl` + `k`        |
| Delete to beginning of line | `ctrl` + `u`        |
| Delete word backwards       | `ctrl` + `w`        |
| Go to start of line         | `⇧` + `fn` + `◀`  |
| Go to end of line           | `⇧` + `fn` + `▶`  |
| Go to previous command      | `▲`                |

## Quick handy commands

Display manual for command

> man _command_

Prefix previous command (bash, zsh)

> _prefix_ !!

Fix previous command in text editor

> fc

## Symbolic links

> ln -s _target_ _link_

```bash
ln -s /var/www sites
```

## Search (grep)

Search for a string in a file.

> grep "_string_" _targetFile_

```bash
grep "John Hammond" staff.txt
```

You can use the `-R` flag to search through files in a directory recursively. The `-E` flag can be used to match regex patterns e.g. `grep -E "local|127" /etc/hosts`. `-n` will show line numbers.

Search recursively through a directory for a whole word.

```bash
grep -Rnw "Triceratops" ./dinos
```

_The `-w` flag is for selecting lines that only contains the **w**hole string._

Standard outputs can also be piped into grep.

> _command_ | grep "_string_"

```bash
ls -la /usr/local/bin/ | grep "php"
```

## Compressing and moving files

Making a tarball.

> tar -czfv _filename_ _pathToTargetDirectory_

```bash
tar -czfv embryos.tar.gz ~/Documents/
```

_The `-c` flag **c**reates an archive, `-z` compresses the archive with g**z**ip, the `-f` allows you to specify the **f**ilename of the archive., and `-v` for **v**erbose mode_

Uncompressing a tarball.

```bash
tar -xzf embryos.tar.gz
```

_The `-x` flag is for e**x**tracting._

## Moving a remote file

> scp _fileToMove_ _user_@_server_:_pathToRemoteDestination_

```bash
scp Whte_rbt.obj dnedry@ingen:~/Desktop
# Files can also be moved from a remote location to a local one.
```

::: tip
If you need to move a directory, add the `-r` flag.
:::

## RSync

Rsync is used to sync filesystems between directories, ideal if one location is remote.

> rsync -av _sourceDir_ _user_@_server_:_targetDir_

```bash
rsync -av ~/Park/ dnedry@biosyn:~/Park
```

_The `-a` flag stands for "**a**rchive" and syncs recursively and preserves symbolic links, special and device files, modification times, group, owner, and permissions. The `-v` flag is for a **v**erbose output (`-n` can be used for a dry-ru**n**)._

::: warning
The 'target dir' is not the same as the 'source dir' respectively, it is it's parent.
:::

Example given the following directory structure on both the source location and the remote, target location.

```
/
└─┬ Park
  └─┬ Zoology
    ├── genomes
    └── research
```

To sync 'genomes' you will need to run:

```bash
rsync -av genomes/ dnedry@biosyn:~/Park/Zoology
```

## Jobs

`jobs` gives you a list of current running or stopped jobs. `ctrl` + `c` kills any current foreground job, `ctrl` + `z` suspends a job

```bash
jobs
Job	Group	State	Command
1	46061	stopped	npm start
```

You can send any job in the `jobs` list to the background by running `bg` followed by the job ID, sending jobs to the foreground is done with `fg`. Machines running Mac OSX will need the job IDs prepended with `%`.

```bash
fg %1
Send job 1, 'npm start' to foreground
```

```bash
bg %1
Send job 1, 'npm start' to background
```

You can send a job directly to the background by appending a `&` after the command when you first run it.

```bash
npm start &
```

## Adding to PATH

A few times you might be told to "add x to your PATH variable", what does this mean? Not quite sure yet, how do you do it? Let me show you. You'll need to have set up your local terminal first.

```bash
vim ~/.paths
# Add it in there with another line: export PATH="$PATH:/your/new/path"
```

Restart your session and that's it basically. It's so that command line commands know where to look I think, most application shortcuts are in `/usr/bin` but I think adding a directory into the PATH var means the shell will look in there for executables.

## Changing the hostname

Change the hostname of the machine in `/etc/hostname` and then restart to make a permanent change.

## Permissions

A file with full, open permissions (777) is represented like this:

<span class="text-red">rwx</span><span class="text-green">rwx</span><span class="text-blue">rwx</span>

- `r`ead (4)
- `w`rite (2)
- e`x`ecute (1)

The first three characters represent permissions on the <span class="text-red">user</span> (`u`), the next three represent permissions on the <span class="text-green">group</span> (`g`), and the last three characters are all <span class="text-blue">other</span> (`o`) permissions.

Permissions can be a bastard, here's how they should look for normal directories and files on a web server:

```bash
ls -la
  drwxr-xr-x    .
  drwxr-xr-x    directory
  -rw-r--r--    file.html
```

Files have 644 permissions and directories have 755.

To change permissions.

```bash
# You can use the permission codes
chmod 644 genomes.sql

# Or you can explicity add/remove role permissions (`u`, `g`, `o`)
# E.g. Add e`x`ecution permission for `u`ser:
chmod u+x genomes.sql
```

To change ownership.

```bash
# File
chown dnedry phones.txt
# Recursive directory
chown -hR dnedry Security/
```

::: tip
`-R` operates on files and directories recursively, `-h` affects each symbolic link instead of any referenced file.
:::

## Processes

List all processes with [process status](<https://en.wikipedia.org/wiki/Ps_(Unix)>), `ps`.

```bash
ps -A
```

Search processes for `Whte_rbt.obj`.

```bash
ps -A | grep Whte_rbt.obj
```

Display all system processes with `top`.

## Generating an SSH key pair

If there if no `.ssh` directory, create one and assign 700 (`drwx------`) permissions.

> ssh-keygen -t rsa -C "_identifier_"

```bash
cd /.ssh
ssh-keygen -t rsa -C "dnedry@email.com"
```

## Pretty Good Privacy (PGP)

For various security functions and en/decrypting files or text chunks, the command line util `gnupg` is used.

To generate a new PGP key, run the following command and follow prompts:

```bash
gpg --full-generate-key
```

Keys can be listed out and identified from the information that is given. You can use `--list-secret-keys` to list any private keys in your GPG keyring.

```bash
gpg --list-keys --keyid-format LONG

/Users/dnedry/.gnupg/pubring.kbx
-----------------------------
pub   rsa4096/0123456789ABCDEF 2019-01-01 [SC]
      123456789ABCDEF123456789ABCDEF123456789A
uid                 [ultimate] DNedry <dnedry@email.com>
sub   rsa4096/9876543210ABCDEF 2019-01-01 [E]
```

To export a public key (`uid` can be the name, email, or key id):

> gpg --export --armor _uid_

Encrypting and decrypting files.

```bash
# Encrypt with someone's public key (including yours)
gpg -e -r jpkey genomes.sql

# Decrypt with your private key
gpg -d genomes.sql.gpg
```

_`-e` flag is for **e**ncrypting, `-d` for **d**ecrypting, and `-r` for specifying the **r**ecipient._

You can encrypt an entire directory by [compressing](#compressing-and-moving-files) it first. You can also use [ZIP](#ZIP)

Raw strings can be en/decrypted from the command line by piping in `echo "Some string"`.

```bash
echo "String to encrypt" | gpg -e
```

## ZIP

To encrypt a directory with a password.

> zip _output_ _target_

```bash
zip -er Crypto.zip Crypto/
```

_`-e` flag is for **e**ncrypting, and `-r` for **r**ecursive (directory)._

## Creating a new Unix user

> sudo adduser _username_

> sudo passwd _username_

```bash
sudo adduser dnedry
sudo passwd dnedry
  Enter new UNIX password:
  Retype new UNIX password:
  passwd: password updated successfully
sudo vim /etc/group

# Add yourself to admin groups.
# (Or you can use `sudo adduser _username_ sudo` to add to sudoers list)

sudo chsh -s /bin/sh dnedry # Changes shell permanently.
```

## Iptables

Iptables is an application program that allows configuration of the Linux kernel firewall tables. The file is located in `/etc/sysconfig/iptables`.

Restart iptables after editing.

```bash
service iptables restart
```

## Disk space

Use `df -h` to display the amount of disk space available on the file system.

```bash
Filesystem           Size   Used  Avail Capacity Mounted on
/dev/disk1s5        234Gi   11Gi   52Gi    17%   /
devfs               262Ki  262Ki    0Bi   100%   /dev
/dev/disk1s1        234Gi  167Gi   52Gi    77%   /System/Volumes/Data
/dev/disk1s4        234Gi  3.0Gi   52Gi     6%   /private/var/vm
map auto_home         0Bi    0Bi    0Bi   100%   /System/Volumes/Data/home
/dev/disk1s3        234Gi  504Mi   52Gi     1%   /Volumes/Recovery
```

::: tip
`-h` is used to display human readable values.
:::
