# Resources

[[toc]]

## Articles

- [How to GraphQL](https://www.howtographql.com/?v1)
- [GraphQL/React course](https://learn.hasura.io/graphql/react/introduction)
- [The Grug Brained Developer](https://grugbrain.dev/)

## Learning

- [Front end masters](https://frontendmasters.com/courses/good-parts-javascript-web/)
- [Google cloud skills boost](https://www.cloudskillsboost.google/)
- [Khan Academy](https://www.khanacademy.org/)
- [RegEx](https://regexlearn.com/)

## Development

- [Phind](https://www.phind.com/); an AI search for devs
- [Web.dev](https://web.dev/)

## JavaScript

- [33 concepts every JS dev should know](https://github.com/leonardomso/33-js-concepts)
- [React internals](http://www.mattgreer.org/articles/react-internals-part-one-basic-rendering/)

## CSS

- [Flex box labs](https://flexboxlabs.netlify.app/)

## Mocking

- [REQ | RES](https://reqres.in) A hosted REST-API ready to respond to your AJAX requests.
