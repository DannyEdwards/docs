# Cloud

[[toc]]

All this crap is constantly changing and new stuff is coming up all the time, so this could be out of date by the time you read this.

## Application build, hosting, and compute

[Netlify](https://www.netlify.com/) and [Vercel](https://vercel.com/) are nice.

Or host something similar yourself with [Coolify](https://coolify.io/) or [Dokku](https://github.com/dokku/dokku).

## Database

Realtime database with [Instant](https://github.com/instantdb/instant).

## Decentralsed web

[IPFS](https://www.ipfs.tech/) is the current tech. [Fleek](https://fleek.co/) is a service that you can use as a "pinned" hosting provider.
