# JavaScript

[[toc]]

## Resources

[Mozilla developer network](https://developer.mozilla.org/) is a great place for web stuff.

## Global object

In a browser, the global object is the `window` object. Anything that is a part of this object on the top level (such as `window.location`) can also be accessed directly, for example `location.search` would get you the `search` property on the `Location` class.

Other global objects are used in other JS contexts like in Node. The global object can be accessed regardless of context with `globalThis`.

## Functional JS

### Shortcuts

`item => item`

```js
array.every((item) => item);
```

```js
array.every(Boolean);
```

### Useful array functions

[`map`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/map),
[`reduce`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/reduce),
[`filter`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter),
[spread operator](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter),
[`includes`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/includes),
[`concat`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/concat),
[`forEach`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach),
[`indexOf`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf),
[`find`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/find),
[`findIndex`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/findIndex),
[`slice`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/slice),
[`some`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/some),
[`every`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/every),
[`flat`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flat),
[`join`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/join).

### `reduce()` function

> _array_.reduce(callback(_accumulator_, _currentValue_, [, _index_[, _array_]] )[, _initialValue_])

Here's a reduce function that doesn't do anything.

```js
[1, 2, 3, 4, 5].reduce((carry, current) => [...carry, current], []);
// [1, 2, 3, 4, 5]
```

Doing `map()` then `filter()`? Let's say we have an array of objects we want to transform and filter based on one property.

```js
[
  { name: "Stegosaurus", isHerbivorous: true },
  { name: "Triceratops", isHerbivorous: true },
  { name: "Dilophosaurus", isHerbivorous: false },
  { name: "Brachiosaurus", isHerbivorous: true },
]
  .map(({ name, isHerbivorous }) => isHerbivorous && { name })
  .filter(Boolean);
/*
[
  { name: "Stegosaurus" },
  { name: "Triceratops" },
  { name: "Brachiosaurus" },
];
*/
```

We can use reduce the same array; concatenating on to it's own accumulator (`carry`) value, starting with an empty array resulting in only one pass.

```js
[
  { name: "Stegosaurus", isHerbivorous: true },
  { name: "Triceratops", isHerbivorous: true },
  { name: "Dilophosaurus", isHerbivorous: false },
  { name: "Brachiosaurus", isHerbivorous: true },
].reduce(
  (carry, { name, isHerbivorous }) =>
    isHerbivorous ? [...carry, { name }] : carry,
  []
);
```

### `slice()` function

> _str_.slice(_beginIndex_[, _endIndex_])

Use `slice()`, it does not change the original array. Slice takes two arguments: The first is required and specifies where to start the selection, the second is optional and specifies where to end the selection.

```js
"We have a T-Rex".slice(10);
// "T-Rex"
```

Ommiting the last argument takes the slice to the end.

You can use negative numbers to come at it from BOTH ways. E.g. use the following to take the last item from an array.

```js
[0, 1, 2, 3, 4].slice(-1);
// [4]
```

## FormData

`FormData` needs to take an `HTMLFormElement` as an argument. You'll have to cast this from a `SubmitEvent` `target` to match types. I really don't know if this is the right way to do things but it's the best I got so far.

```ts
function addSpot(event: SubmitEvent) {
  const formData = new FormData(event.target as HTMLFormElement);
  // ...
}
```

## Inline documentation

Use [TypeScript](https://www.typescriptlang.org/docs/handbook/intro.html).

## Node

I install node with [pnpm](https://pnpm.io/). It's a more efficient npm alternative and you can install and manage node versions with it.

```bash
pnpm env use --global lts
```

::: tip
To use `pnpm env` you may need to switch to another shell like `zsh`. Check the list of installed envs with `pnpm env list`.
:::

### Npm

To install an NPM package GitLab repository.

```bash
npm install git+ssh://git@gitlab.com:DannyEdwards/cachefetch.git
```
