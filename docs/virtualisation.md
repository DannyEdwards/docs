# Virtualisation

[[toc]]

## Docker

Carrying out command line operations on a [Docker](https://www.docker.com/) container. To list all current docker containers use `docker ps`.

Execute arbitrary commands with `docker exec`.

> docker exec _containerId_ _command_

E.g. perform a mysql database dump

```bash
docker exec jp_db /usr/bin/mysqldump -u root users > users.sql
```

Use `docker cp` to copy files or entire directory trees. This can also be used to copy from local machine to the container.

```bash
docker cp jp_db:/bin/phones ~/Desktop
```
