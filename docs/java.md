# Java

[[toc]]

No, I did not forget to write "Script", this is about Java.

## Resources

[Java downloads](https://www.oracle.com/java/technologies/downloads/). If you need an older version, "something search" it and download from the Oracle archive.

## See which JDKs are installed

```bash
/usr/libexec/java_home -V
```

## Switch Java versions

To switch to Java v11 (for example):

```bash
export JAVA_HOME=$(/usr/libexec/java_home -v11)
```

Check the current version with `java -version`. For a permanent change add the export to your bash config, though this is probably not recommended as older versions of Java can have security vulnerabilities.

::: tip
If you are using [`.dotfiles`](https://gitlab.com/DannyEdwards/dotfiles) you can use the `set_java_version` function.

```bash
set_java_version 11
```

:::
