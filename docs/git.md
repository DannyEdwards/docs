# Git

[[toc]]

## Workflow

As well as Git providing version control, it should also be used as remote backup to save your work outside of your machine.

Feature branches should always be created.

Commits should be functional and as small as possible.

When working on something, particularity if it's a large feature, it's a good idea to create a single commit and frequently `--amend` and force push to that same commit as a work in progress.

```bash
git commit -m "wip: some new feature"
git push
```

Continue working, creating a remove "save" as often as you like:

```bash
git commit --amend
git push --force-with-lease
```

::: tip
If you are using the [`.dotfiles`](https://gitlab.com/DannyEdwards/dotfiles) project you can use `git push-fwl` as a shortcut for a force push with lease.
:::

When work has finished and the feature is complete, unstage the entire commit.

```bash
git reset HEAD~ # `HEAD~` has the same effect as `HEAD~1`
```

Then you can re-add all of your files and build up the appropriate commit chunks and messages as you wish. Finally force push the final commits.

## Creating a branch

> git checkout -b _branchName_

You can set upstream by passing the `-u` argument the first time you push.

```bash
git push -u origin HEAD
```

Or just explicity set the upstream.

> git branch --set-upstream-to=origin/_branchName_ _branchName_

::: tip
Other [`.dotfiles`](https://gitlab.com/DannyEdwards/dotfiles) shortcuts include `s` (`status`), `co` (`checkout`), and `b` (`branch`). You can also set upstream and push with `push-u`.
:::

## Renaming a branch

If you are currently in the branch you wish to rename

> git branch -m _newName_

Otherwise

> git branch -m _oldName_ _newName_

## Rebasing

Interactive rebase helps you rewrite your commit history (Where _n_ is the number of commits up from `HEAD`)

> git rebase -i HEAD~_n_

```bash
# Commands:
#  p, pick = use commit
#  r, reword = use commit, but edit the commit message
#  e, edit = use commit, but stop for amending
#  s, squash = use commit, but meld into previous commit
#  f, fixup = like "squash", but discard this commit's log message
#  x, exec = run command (the rest of the line) using shell
```

File is executed from top to bottom.

"Previous commit" means the commit on the line before. E.g do this:

```bash
p 89e2b15 Fix: Clearing eslint red-flags
f d74971f Fix: Clearing eslint red-flags # (discard this)
```

"Squash up"/"Fixup... up"

::: danger
Rebasing can potentially cause serious problems, make sure you only really do it on your own branches.
:::

### Staging (adding)

You can do interactive staging with this command.

```bash
git add -i
```

This fires up a useful CLI that you can do loads of stuff with. You will usually be doing **u**pdates, **p**atches, looking at **d**iffs, and probably **r**everting.

The **p**atching method is a good way to run through the changes and add hunky "hunks" bit by bit. You can add a patch without going through the interactive staging with `git add -p`.

### Edit

Edit your files as necessary. You can even unstage everything in the current commit with `git reset HEAD~`.

```bash
git commit --amend # To edit the commit message
git rebase --continue # Conclude rebase
```

### Push

> git push --force-with-lease origin _branchName_

::: tip
If you are using the [`.dotfiles`](https://gitlab.com/DannyEdwards/dotfiles) project you can use `git push-fwl` as a shortcut for a force push with lease.
:::

## Tags

List all tags.

```bash
git tag
```

Create a lightweight tag.

```bash
git tag v1.0.0
```

By default, `git push` does not transfer tags to the remote, you have to do it manually.

```bash
git push origin --tags
```

Delete a tag

```bash
git tag -d v1.0.0
```

Checkout a tag

```bash
git checkout v1.0.0
```

## Shit's really effed up (reflog)

How messed up? This will reset your branch back to the state on the remote:

```bash
git reset --hard origin/feature/whterbt
# Where feature/whterbt is the branch name
```

Worse than that? Ok, we're getting out the `reflog`. Use `reflog` to look at git's reference history (basically anything that you'd do moving around). You can then checkout at any point in the log.

<code-group>
<code-block title="Fish">
```bash
git reflog
# See where you messed up
git checkout "HEAD@{7}"
```
</code-block>

<code-block title="Bash">
```bash
git reflog
# See where you messed up
git checkout HEAD@{7}
```
</code-block>
</code-group>

This will leave you with a "detached head", just checkout a new branch from here and you're sorted.

## Submodules

To add a submodule to a project execute the following command:

> git submodule add _repositoryAddress_ [_pathToNewDir_]

When you clone a repository with submodules you will have to clone in the following way:

> git clone _repositoryAddress_ --recursive

::: tip
If you have already cloned the repo you will need to run the following after the fact: `git submodule update --init`.
:::

## Multiple accounts

If you have multple SSH keys and multiple Git accounts; adjust the `~/.ssh/config` file to something like the following:

```bash
Host *
  AddKeysToAgent yes
  UseKeychain yes
  IdentityFile ~/.ssh/id_ed25519 # Personal/default

Host ingen.github.com
  HostName github.com
  User git
  IdentityFile ~/.ssh/id_ed25519_ingen
```

Then when you clone a repo from a separate organisation use the custom host.

```bash
git clone git@ingen.github.com:ingen/unix-os
```

## Using USB as a remote repo

First cd into your USB drive and create a directory to store the remote.

```bash
cd /dev/sda1/repositories
mkdir my-repo.git

# Now clone your repo into the new directory as a *bare* repo.

git clone --bare ~/www/my-repo my-repo.git
```

Now, from your local, working repo set up the new USB remote.
