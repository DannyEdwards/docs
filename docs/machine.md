# Your machine

[[toc]]

## Mac OSX

### Installing the Homebrew Package Manager

[Homebrew](https://brew.sh/) is a handy package manager for general Mac software/packages. I don't tend to use it that much any more really.

### Improve UI

#### Remove show delay on the Dock

```bash
# Delay
defaults write com.apple.dock autohide-delay -float 0
# Animation time
defaults write com.apple.dock autohide-time-modifier -float 0
# Restart
killall Dock
```

Undo.

```bash
defaults delete com.apple.dock autohide-delay &&
defaults delete com.apple.dock autohide-time-modifier &&
killall Dock
```

#### Increace key repeat rate

```bash
defaults write NSGlobalDomain KeyRepeat -int 1
```

You have to log out and back in again for this one. Default setting is `2`.

#### Allow apps downloaded from "Anywhere"

On macOS Monterey this is locked down in System Preferences > Security & Privacy, you can turn the gatekeeper off with:

```bash
sudo spctl --master-disable
```

## Linux Mint

[Linux Mint](https://linuxmint.com/) 19 (Cinnamon).

## Windows 10

To stop Windows constantly communicating with daddy Gates and nuking your bandwidth, type `Services` into the start box and disable/stop the following processes.

- Background Intelligent Transfer Service
- Windows Update
