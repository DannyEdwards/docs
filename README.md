---
home: true
heroText: 📓 Dan's docs
tagline: Notes and documentation for development and general computer stuff.
actionText: Hold on to your butts →
actionLink: /docs/machine.html
---

::: tip
For instructions, required parameters are in _italics_, and optional parameters are in square brackets (`[]`).
:::

To serve this project locally, run:

```bash
npm start
```
